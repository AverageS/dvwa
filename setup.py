import requests
import lxml.html
import sys
url =  "http://www.example.com/servlet/av/ResultTemplate=AVResult.html"

BASE_URL = sys.argv[1]

s = requests.Session()
r = s.get(BASE_URL+'login.php', stream=True)
r.raw.decode_content = True
login_csrf_xpath = '//*[@id="content"]/form/input'

tree =  lxml.html.parse(r.raw)
csrf_token = tree.xpath(login_csrf_xpath)[0].value
print(csrf_token)

r = s.post(BASE_URL+'login.php', data={"username": "admin", "password": "password", "Login": "Login", "user_token": csrf_token})
r = s.get(BASE_URL+'setup.php', stream=True)
r.raw.decode_content = True
setup_csrf_xpath = '//*[@id="main_body"]/div[1]/form/input[2]'

tree =  lxml.html.parse(r.raw)
csrf_token = tree.xpath(setup_csrf_xpath)[0].value
print(csrf_token)

r = s.post(BASE_URL+'setup.php', data={'create_db':'Create / Reset Database', "user_token": csrf_token})
print(r.status_code)

r = s.get(BASE_URL+'security.php', stream=True)
r.raw.decode_content = True
security_csrf_xpath = '//*[@id="main_body"]/div/form/input[2]'

tree =  lxml.html.parse(r.raw)
csrf_token = tree.xpath(setup_csrf_xpath)[0].value
print(csrf_token)

r = s.post(BASE_URL+'security.php', data={'security': 'low', 'seclev_submit': 'Submit', 'user_token': csrf_token})
print(r.status_code)



